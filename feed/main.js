global.__base = __dirname + '/';

var express= require('express'),
    path = require('path'),
    app	   = express();

//public directory
// app.use('/public', express.static('public'));

app.use(express.static(path.join(__base,'client/css')));
app.use(express.static(path.join(__base,'client/js/static')));


app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
    });

app.listen(3000);

console.log('Listening on port 3000');
