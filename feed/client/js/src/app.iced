$       = require 'jquery'
_       = require 'lodash'
getUrls = require('linkify-it')()
m       = require './html'
 
feedGetUrl = (params) ->
 "https://api.vk.com/api.php?oauth=1&method=wall.get&domain=extrawebdev&count=#{params.count}&offset=#{params.offset}"

storeUrlCounters =
 count: 5 # const
 offset: 0

getFeed = (cb) ->
 await $.ajax
          url : feedGetUrl storeUrlCounters
          type : "GET"
          dataType : "jsonp"
          success: defer res

 if res
  # utils:
  # check exist url
  getUrl = (text) -> _.first(getUrls.match(text))?.url
  # wrap image in markup
  getImage = (src) ->
   m.tag('img').as({'.': "pure-img", 'src': src, 'alt': '', 'style': "width: 100%"})
          
  # create feed blocks 
  feedItems =
   (for i in _.tail res.response
     m.tag('div').as({'.': "pure-g", 'data-id': "#{i.id}"},
      m.tag('div').as({'.': "pure-u-1-1 feed-item"},
       m.tag('div').as({'.': "feed-item-id"}, "post-id: ##{i.id}"),
       m.tag('div').as({'.': "feed-item-link"},
        m.tag('a').as({'href': getUrl(i.text)},
         if not _.isEmpty(getUrl(i.text))   
          getUrl(i.text)))
       m.tag('div').as({'.': "feed-item-photo"},
         if i.attachment?.photo
          getImage i.attachment.photo.src_big)))).join('')
 
  # update store
  storeUrlCounters.offset = storeUrlCounters.offset + storeUrlCounters.count 
 
  cb feedItems
  
# on load document
$ ->
 onLoadLayots =
  m.tag('div').as({'.': "pure-g"},
   m.tag('div').as({'.': "pure-u-1-5"})
   m.tag('div').as({'.': "pure-u-3-5", "#": "feed-list"})
   m.tag('div').as({'.': "pure-u-1-5"}))

 # create start layots        
 $('body').html onLoadLayots
 # & feel feed
 getFeed((feed) ->
          $("#feed-list").html feed)

# listen scroll bottom
$(window).scroll ->
 if $(window).scrollTop() + $(window).height() is  $(document).height()
  # feel feed
  getFeed (feed) ->
    $("#feed-list").append feed


           

           





