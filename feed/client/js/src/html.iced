_ = require 'lodash'


html =
 tag: (tag) ->
  as: (attrs, value...) ->
   attrsRender = ->
    (for key, values of attrs
    
      keyTransform = (key) ->
       switch _.first _.trim(key)
        when '.'
         ' class'
        when '#'
          ' id'
        else " #{key}"
                
      valueTransform = (value) ->
       if _.isString(value)
         "\"#{value}\""                 
       else value
      
      if _.first(_.trim(key)) is '/' 
       " #{_.rest(_.trim(key)).join('')}"        
      else
       "#{keyTransform(key)}=#{valueTransform(values)}").join ''
  
   if _.first(_.trim(tag)) is '/'
    "<#{_.rest(_.trim(tag)).join('')}#{attrsRender()}>#{value.join('')}"
   else                              
    "<#{tag}#{attrsRender()}>#{value.join('')}</#{tag}>"

module.exports = html

# console.log html.tag('/div').as({'#':1, '.': 'class1', 'data-i': true, data: "5",  '/checked', '/required'}, 1.3, 5, html.tag('/div').as(null),'10')




