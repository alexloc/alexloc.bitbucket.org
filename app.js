var express= require('express'),
    path = require('path'),
    app	   = express();

//public directory
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function (req, res) {
    res.sendfile('index.html');
    });

app.listen(3000);

console.log('Listening on port 3000');
